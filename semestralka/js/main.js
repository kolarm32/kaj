class SmartWallet {
    constructor() {
        this.records = JSON.parse(localStorage.getItem("records")) || [];
        this.categories = null;
        this.showRecords();
        this.sound = new Audio("./audio/sound.mp3");

        document.getElementById("overviewBtn").addEventListener('click', e => this.goToOverview());
    }

    setCategories(categories) {
        this.categories = categories;
    }

    // scrolls down to the overview area
    goToOverview (e) {
        let div = document.querySelector("#walletStats");
        div.scrollIntoView({ behavior: 'smooth', block: 'start'});
        this.refreshChart();
    }

    refreshChart() {
        this.addUpTotalPrice();
        this.updateChart();
    }

    // renders chart
    // Chart JS framework
    updateChart () {
        var canvas = document.getElementById('myChart');
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        var myChart = new Chart(canvas, {
            type: 'pie',
            data: {
                labels: ['Entertainment', 'Shopping', 'Food', 'Travel', 'Health&Fitness'],
                datasets: [{
                    data: [this.categories[0].sum, this.categories[1].sum, this.categories[2].sum, this.categories[3].sum, this.categories[4].sum],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false
            }
        });
    }

    // adds up total money spent by each category
    addUpTotalPrice () {
        for (let index = 0; index < this.categories.length; index++) {
            this.categories[index].sum = 0;
        }
        for (const record in smartWallet.records) {
            for (const category in smartWallet.categories) {
                if (smartWallet.categories[category].name.includes(smartWallet.records[record].category.replace(" ", ""))) {
                    smartWallet.categories[category].sum += parseInt(smartWallet.records[record].price);
                }
            } 
        }
        this.firstRun = false;
    }

    // adds a new record to the local storage
    addRecord (record) {
        this.sound.pause();
        this.sound.currentTime = 0;
        this.records.push(record)
        localStorage.setItem("records", JSON.stringify(this.records));
        this.sound.play();
        this.refreshChart();
    }

    // displays all records saved in the local storage on page
    showRecords () {
        const target = document.querySelector('#recordsTable');

            for (const record of this.records) {
                const tr = document.createElement('tr');
                var newCell1  = tr.insertCell(0);
                var newCell2  = tr.insertCell(1);
                var newCell3  = tr.insertCell(2);
                var newCell4  = tr.insertCell(3);
                var newCell5  = tr.insertCell(4);
                var newCell6  = tr.insertCell(5);
                newCell6.className = "recordIdCol";
                let date = document.createElement("input");
                date.type = "date";
                date.value = record.date;
                date.disabled = true;
                date.name = "rowDate";

                let price = document.createElement("input");
                price.type = "text";
                price.value = record.price;
                price.disabled = true;
                price.name = "rowPrice";
                let deleteButton = document.createElement("button");
                deleteButton.innerHTML = "Remove";
                deleteButton.onclick = this.removeRecord;

                newCell1.appendChild(date);
                newCell2.appendChild(new Text(record.category));
                newCell3.appendChild(price);
                newCell4.appendChild(new Text("CZK"));
                newCell5.appendChild(deleteButton);
                newCell6.appendChild(new Text(record.id));
                target.appendChild(tr);
            }
    }

    // removes record from the local storage
    removeRecord () {
        let td = event.target.parentNode; 
        let tr = td.parentNode;
        let id = tr.cells[5].innerText;
        for (let index = 0; index < smartWallet.records.length; index++) {
            if (smartWallet.records[index].id == id) {
                smartWallet.records.splice(index, 1);
            }
        }
        tr.parentNode.removeChild(tr);
        localStorage.setItem("records", JSON.stringify(this.records));
        smartWallet.refreshChart();
    }
}

class Category {
    constructor(name) {
        this.name = name;
        this.sum = 0;
    }
}

let categoriesList = [];
category1 = new Category("Entertainment");
category2 = new Category("Shopping");
category3 = new Category("Food");
category4 = new Category("Travel");
category5 = new Category("Health&Fitness");
categoriesList.push(category1, category2, category3, category4, category5);

const categoriesFill = document.querySelectorAll('.categoryFill');
const categories = document.querySelectorAll('.category');
const tableBody = document.querySelector('#mainTable');

// Fill listeners
for(const category of categoriesFill) {
    category.addEventListener('dragstart', dragStart);
    category.addEventListener('dragend', dragEnd);
}

let categoryText = "";
// Drag Functions
function dragStart() {
    this.className += ' hold';
    categoryText = this.innerText;
    setTimeout(() => this.className = 'invisible, 0')
}

function dragEnd() {
    this.className = 'fill';
}

tableBody.addEventListener('dragover', dragOver);
tableBody.addEventListener('dragenter', dragEnter);
tableBody.addEventListener('dragleave', dragLeave);
tableBody.addEventListener('drop', dragDrop);

function dragOver(event) {
    event.preventDefault();
}

function dragEnter(event) {
    event.preventDefault();
    this.className += ' hovered';
}

function dragLeave() {
}

function dragDrop(event, categoryText) {
    this.className -= 'hovered';
    addRow(this.innerText);
}

let errorMsg = document.createElement("span");
let confirmButton;

function addRow() {
    checkIfSaved();

    var tableRef = document.getElementById('mainTable').getElementsByTagName('tbody')[0];
    var newRow   = tableRef.insertRow();
    unsavedRecord = true;

    var newCell1  = newRow.insertCell(0);
    var newCell2  = newRow.insertCell(1);
    var newCell3  = newRow.insertCell(2);
    var newCell4  = newRow.insertCell(3);
    var newCell5  = newRow.insertCell(4);
    var newCell6  = newRow.insertCell(5);
    newCell6.className = "recordIdCol"

    let date = document.createElement("input");
    date.type = "date";
    date.name = "rowDate";

    let price = document.createElement("input");
    price.type = "text";
    price.name = "rowPrice";

    let deleteButton = document.createElement("button");
    deleteButton.className = "btn btn-info";
    deleteButton.innerHTML = "Remove";
    deleteButton.onclick = removeTableRow;

    newCell1.appendChild(date);
    newCell2.appendChild(new Text(categoryText));
    newCell3.appendChild(price);
    newCell4.appendChild(new Text("CZK"));
    newCell5.appendChild(deleteButton);
    newCell6.appendChild(new Text(recordId));

    errorMsg = document.createElement("span");
    document.getElementById('mainTable').appendChild(errorMsg);

    if (document.getElementById("confirmButton") !== null) {
        document.getElementById("confirmButton").remove();
    }
    confirmButton = document.createElement("button");
    confirmButton.id = "confirmButton";
    confirmButton.className = "btn btn-primary"
    confirmButton.innerHTML = "Confirm";
    document.getElementById('controlArea').appendChild(confirmButton);

    document.getElementById('mainTable').getElementsByTagName('input')[document.getElementById('mainTable').getElementsByTagName('input').length - 2].value = new Date().toDateInputValue();
    confirmButton.addEventListener("click", validateData);
}

function removeTableRow() {
    smartWallet.removeRecord();
}

function checkIfSaved() {
    if (records.length == 0) {
        return;
    }
    if (unsavedRecord == true) {
        var tableRef = document.getElementById('mainTable').getElementsByTagName('tbody')[0];
        tableRef.deleteRow(-1);
    }
}

// validates user input
function validateData() {
    let lastRow = document.getElementsByTagName("tr");
    let re = /^[0-9]*$/;
    let date = lastRow[lastRow.length - 1].cells[0].firstChild.value;
    let category = lastRow[lastRow.length - 1].cells[1].firstChild.nodeValue;
    let price = lastRow[lastRow.length - 1].cells[2].firstChild.value;
    if (validatePrice(price) == false) {
        errorMsg.innerText = "Enter a valid number, greater than 1."
    }
    else {
        lastRow[lastRow.length - 1].cells[0].firstChild.disabled = true;
        lastRow[lastRow.length - 1].cells[2].firstChild.disabled = true;
        document.getElementById('confirmButton').remove();
        errorMsg.innerText = "";
        createRecord(date, category, price);
    }
}

// Regex functions
function validatePrice(price) {
    let regExp = /^[1-9][0-9]*$/;
    return regExp.test(price);
}

// Set current date
Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

let unsavedRecord = false; 
let records = []; // list of records
function createRecord(date, category, price) {
    let record = new Record(date, category, price);
    records.push({record});
    record.setSaved(true);
    unsavedRecord = false;
    smartWallet.addRecord(record);
}


// Records 
let recordId = 0;
function Record(date, category, price) {
    this.id = recordId++;
    this.date = date;
    this.category = category;
    this.price = price;
    this.saved = false;

    this.getId = function() {
        return this.id;
    }
    this.getDate = function() {
        return this.date;
    }
    this.getCategory = function() {
        return this.category;
    }
    this.getPrice = function() {
        return this.price;
    }
    this.setSaved = function(newSaved) {
        this.saved = newSaved;
    }
    this.isSaved = function() {
        return this.saved;
    }
}

function updateOnlineStatus(event) {
    var status = document.getElementById("status");
    var condition = navigator.onLine ? "online" : "offline";

    status.className = condition;
    status.innerHTML = condition.toUpperCase();
    console.log(status.innerHTML);
}

window.addEventListener('online',  updateOnlineStatus);
window.addEventListener('offline', updateOnlineStatus);

let smartWallet = new SmartWallet();
smartWallet.setCategories(categoriesList);