Cílem této semestrální práce bylo vytvořit jednoduchou webovou aplikaci, která slouží pro sledování útraty peněz a vykresluje podle toho grafy.

Ovládání:
Uživatel vytvoří nový záznam přetahnutí libovolné kategorie do tabulky (pomocí drag'n'drop). 
Následně vybere datum a vyplní množství peněz, které utratil a potvrdí kliknutím na tlačítko 'Confirm'.
Po úspěšném vložení nového záznamu se přehraje kratký zvuk signalizující úspěšné přidání.
Vše se ukládá do lokální paměti a jakoukoliv položku je možné odstranit kliknutím na 'Remove' v požadovaném řádku.
Kliknutím na 'Go to overview' se dostáváme do části, která z uložených dat vykreslí graf podle velikosti útraty v jednotlivých kategoriích. Ve spodním levém rohu můžeme vidět, zda jsme aktualně pripojeni k internetu.